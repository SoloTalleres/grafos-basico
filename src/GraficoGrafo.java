import com.mxgraph.layout.mxGraphLayout;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.view.mxGraph;
import javax.swing.JFrame;

public class GraficoGrafo {
    public static void main(String[] args) {
        // Crear el grafo
        mxGraph grafo = new mxGraph();
        Object parent = grafo.getDefaultParent();

        // Agregar vértices al grafo y asignar etiquetas
        grafo.getModel().beginUpdate();
        try {
            Object v1 = grafo.insertVertex(parent, null, "Vertice 1", 20, 20, 80, 30);
            Object v2 = grafo.insertVertex(parent, null, "Vertice 2", 200, 150, 80, 30);
            Object v3 = grafo.insertVertex(parent, null, "Vertice 3", 350, 50, 80, 30);

            // Agregar aristas al grafo y asignar etiquetas
            grafo.insertEdge(parent, null, "Arista 1-2", v1, v2);
            grafo.insertEdge(parent, null, "Arista 2-3", v2, v3);
            grafo.insertEdge(parent, null, "Arista 3-1", v3, v1);
        } finally {
            grafo.getModel().endUpdate();
        }

        // Configurar estilo de las aristas (etiquetas)
        Object[] edges = grafo.getChildEdges(parent);
        for (Object edge : edges) {
            grafo.getModel().setStyle(edge, "labelPadding=5");
        }

        // Crear el componente de visualización del grafo
        mxGraphComponent graphComponent = new mxGraphComponent(grafo);

        graphComponent.stopEditing(false);

        // Configurar la ventana de visualización
        JFrame frame = new JFrame();
        frame.getContentPane().add(graphComponent);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        frame.setVisible(true);
    }
}
